<?php

namespace _\page\image {
    function crop($image, array $lot = []) {
        if (!\is_string($image) || \count($lot) === 0) {
            return $image;
        }
        $width = \round($lot[0]);
        $height = \round($lot[1] ?? $width);
        $x = \Path::X($image, 'jpg');
        $name = ($width === $height ? $width : $width . DS . $height) . '.' . $x;
        $path = \To::path(\URL::long($image));
        if (\strpos($path, ROOT . DS) === 0) {
            $path = \Path::F($path) . DS . $name; // local
        } else {
            $path = ASSET . DS . $x . DS . 't' . DS . \dechex(\crc32($image)) . DS . $name;
        }
        if (\is_file($path)) {
            return \To::URL($path);
        } else if ($image && \extension('image') !== null) {
            $blob = new \Image($image);
            $blob->crop($width, $height)->saveTo($path);
            return \To::URL($path);
        }
        return $image;
    }
}

namespace _\page\images {
    function crop(array $images, array $lot = []) {
        foreach ($images as &$image) {
            $image = \_\page\image\crop($image);
        }
        return $images;
    }
}

namespace _\page {
    function image($image, array $lot = []) {
        // Skip if `image` data already set!
        if ($image) {
            return $image;
        // Get URL from `content` data
        } else if ($content = $this->get('content')) {
            $try = \plugin('image')['try'];
            // Get URL from `<img>` tag
            if (\strpos($content, '<img ') !== false) {
                $parser = new \DOMDocument('1.0', 'UTF-8');
                $parser->strictErrorChecking = false;
                $parser->validateOnParse = true;
                @$parser->loadHTML($content); // TODO
                $node = $parser->getElementsByTagName('img')->item(0);
                return $node ? ($node->getAttribute('src') ?: null) : null;
            // Get URL from `<video>` tag
            } else if (\strpos($content, '<video ') !== false) {
                $parser = new \DOMDocument('1.0', 'UTF-8');
                $parser->strictErrorChecking = false;
                $parser->validateOnParse = true;
                @$parser->loadHTML($content); // TODO
                $node = $parser->getElementsByTagName('video')->item(0);
                return $node ? ($node->getAttribute('poster') ?: null) : null;
            // Get Vimeo image URL
            } else if (\strpos($content, 'vimeo.com') !== false && !empty($try['vimeo'])) {
                if (\preg_match('#vimeo\.com/(?:[a-z]*/)*([0-9]{6,11})#', $content, $m)) {
                    if ($json = \HTTP::fetch('https://vimeo.com/api/v2/video/' . $m[1] . '.json')) {
                        $json = \json_decode($json, true);
                        return $json[0]['thumbnail_medium'] ?? null;
                    }
                }
            // Get YouTube image URL
            } else if (\strpos($content, 'youtu') !== false && !empty($try['youtube'])) {
                if (\preg_match('#(?:youtube(?:-nocookie)?\.com/(?:[^"\'>&?/ ]+/[^"\'>&?/ ]+/|(?:v|e(?:mbed)?)/|[^"\'>&?/ ]*[?&]v=)|youtu\.be/)([^"\'>&?/ ]{11})#i', $content, $m)) {
                    return 'https://img.youtube.com/vi/' . $m[1] . '/mqdefault.jpg';
                }
            }
        }
        // Get URL from `css` data
        if ($css = $this->get('css')) {
            if (\strpos($css, 'url(') !== false && \preg_match('#\burl\(([\'"]?)(.+?\.(?:' . \strtr(IMAGE_X, ',', '|') . '))\1\)#', $css, $m)) {
                return $m[2];
            }
        }
    }
    function images($images, array $lot = []) {
        // Skip if `images` data already set!
        if ($images) {
            return $images;
        // Get URL from `content` data
        } else if ($content = $this->get('content')) {
            $try = \plugin('image')['try'];
            // Get URL from `<img>` tag
            if (\strpos($content, '<img ') !== false) {
                $parser = new \DOMDocument('1.0', 'UTF-8');
                $parser->strictErrorChecking = false;
                $parser->validateOnParse = true;
                @$parser->loadHTML($content); // TODO
                $nodes = [];
                foreach ($parser->getElementsByTagName('img') as $node) {
                    if ($src = $node->getAttribute('src')) {
                        $nodes[] = $src;
                    }
                }
                return $nodes;
            // Get URL from `<video>` tag
            } else if (\strpos($content, '<video ') !== false) {
                $parser = new \DOMDocument('1.0', 'UTF-8');
                $parser->strictErrorChecking = false;
                $parser->validateOnParse = true;
                @$parser->loadHTML($content); // TODO
                $nodes = [];
                foreach ($parser->getElementsByTagName('video') as $node) {
                    if ($poster = $node->getAttribute('poster')) {
                        $nodes[] = $poster;
                    }
                }
                return $nodes;
            // Get Vimeo image URL
            } else if (\strpos($content, 'vimeo.com') !== false && !empty($try['vimeo'])) {
                if (\preg_match_all('#vimeo\.com/(?:[a-z]*/)*([0-9]{6,11})#', $content, $m)) {
                    return \array_filter(\map($m[1], function($id) {
                        if ($json = \HTTP::fetch('https://vimeo.com/api/v2/video/' . $id . '.json')) {
                            $json = \json_decode($json, true);
                            return $json[0]['thumbnail_medium'] ?? null;
                        }
                        return null;
                    }));
                }
            // Get YouTube image URL
            } else if (\strpos($content, 'youtu') !== false && !empty($try['youtube'])) {
                if (\preg_match_all('#(?:youtube(?:-nocookie)?\.com/(?:[^"\'>&?/ ]+/[^"\'>&?/ ]+/|(?:v|e(?:mbed)?)/|[^"\'>&?/ ]*[?&]v=)|youtu\.be/)([^"\'>&?/ ]{11})#i', $content, $m)) {
                    return \map($m[1], function($id) {
                        return 'https://img.youtube.com/vi/' . $id . '/mqdefault.jpg';
                    });
                }
            }
        }
        // Get URL from `css` data
        if ($css = $this->get('css')) {
            if (\strpos($css, 'url(') !== false && \preg_match_all('#\burl\(([\'"]?)(.+?\.(?:' . strtr(IMAGE_X, ',', '|') . '))\1\)#', $css, $m)) {
                return $m[2];
            }
        }
    }
    \Hook::set('page.image', __NAMESPACE__ . "\\image", 2.1, 1);
    \Hook::set('page.images', __NAMESPACE__ . "\\images", 2.1, 1);
    \Hook::set('page.image', __NAMESPACE__ . "\\image\\crop", 2.2, 1);
    \Hook::set('page.images', __NAMESPACE__ . "\\images\\crop", 2.2, 1);
}