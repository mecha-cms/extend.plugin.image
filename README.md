Image Property Plugin for Mecha
===============================

> Add `image` and `images` property to the page output.

This plugin adds `image` and `images` property to the current page output. The `image` property will contain the first image URL found in the current page content, and the `images` property will contain list of images URL found in the current page content.

This plugin will skip the parsing process if you set `image` and `images` property explicitly to the page header.